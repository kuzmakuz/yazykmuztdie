<?php

require_once 'GoogleTranslateForFree.php';

# Принимаем запрос
$data = json_decode(file_get_contents('php://input'), TRUE);
//file_put_contents('file.txt', '$data: '.print_r($data, 1)."\n", FILE_APPEND);


//https://api.telegram.org/bot5796611983:AAH-EpkzQilxedLJPkJh8EOlqLpS5Qffu4A/setwebhook?url=https://botfarm.top/bots/yazykmustdie/index.php

# Обрабатываем ручной ввод или нажатие на кнопку
$data = $data['callback_query'] ? $data['callback_query'] : $data['message'];

# Важные константы
define('TOKEN', '5796611983:AAH-EpkzQilxedLJPkJh8EOlqLpS5Qffu4A');

# Записываем сообщение пользователя
$message = mb_strtolower(($data['text'] ? $data['text'] : $data['data']),'utf-8');
$message_orig = $data['text'] ? $data['text'] : $data['data'];
# Vybiraem menu
$file = $data['chat']['id'] . '.json'; // Zadaem file menu
$menu = json_decode(file_get_contents($file), TRUE);

$time = date('H:m:s');

$method = 'sendMessage';

if ($message == "💩🤢🤮 ➡️➡️➡️ 🇺🇦🇺🇦🇺🇦") {
    $message = 'ru-ua';
} elseif ($message == "🇺🇦🇺🇦🇺🇦 ➡️➡️➡️ 💩🤢🤮") {
    $message = 'ua-ru';
}

switch ($message)
{
    case '/start':
        $send_data = [
            'text'   => 'Ласкаво проcимо!!! '. PHP_EOL . 'Це мініатюрний кацапсько-український та україно-кацапький перекладач' . PHP_EOL . 'Стане у нагоді при переході на солов\'їну мову 😊🤗🇺🇦.'. PHP_EOL . 'Будь ласка, оберіть напрямок перекладу.',
            'reply_markup' => [
                'resize_keyboard' => true,
                'keyboard' => [
                    [
                        ['text' => '🇺🇦🇺🇦🇺🇦 ➡️➡️➡️ 💩🤢🤮']
                    ],
                    [
                        ['text' => '💩🤢🤮 ➡️➡️➡️ 🇺🇦🇺🇦🇺🇦']
                    ]
                ]
            ]
        ];
        zapisJson('start');
        break;

    case 'ua-ru':
        $send_data = [
            'text'   => 'Введіть слово або тект',
            'reply_markup' => [
                'resize_keyboard' => true,
                'keyboard' => [
                    [
                        ['text' => '💩🤢🤮 ➡️➡️➡️ 🇺🇦🇺🇦🇺🇦']
                    ]

                ]
            ]
        ];
        zapisJson('uaru');
        break;

    case 'ru-ua':
        $send_data = [
            'text'   => 'Введіть щоcь мовою лайна',
            'reply_markup' => [
                'resize_keyboard' => true,
                'keyboard' => [
                    [
                        ['text' => '🇺🇦🇺🇦🇺🇦 ➡️➡️➡️ 💩🤢🤮']
                    ]

                ]
            ]
        ];
        zapisJson('ruua');
        break;

    default:
        if ($menu!='uaru' && $menu!='ruua')
        {
            $send_data = [
                'text'   => 'Оберіть напрямок перекладу',
                'reply_markup' => [
                    'resize_keyboard' => true,
                    'keyboard' => [
                        [
                            ['text' => '🇺🇦🇺🇦🇺🇦 ➡️➡️➡️ 💩🤢🤮']
                        ],
                        [
                            ['text' => '💩🤢🤮 ➡️➡️➡️ 🇺🇦🇺🇦🇺🇦']
                        ]
                    ]
                ]
            ];
        } elseif ($menu=='uaru') {

            $tr = new GoogleTranslateForFree();
            $result = $tr->translate('uk', 'ru', $message_orig, 5);

            $send_data = [
                'text'   => $result,
                'reply_markup' => [
                    'resize_keyboard' => true,
                    'keyboard' => [
                        [
                            ['text' => '💩🤢🤮 ➡️➡️➡️ 🇺🇦🇺🇦🇺🇦']
                        ]
    
                    ]
                ]
            ];

        } elseif ($menu=='ruua') {
            $tr = new GoogleTranslateForFree();
            $result = $tr->translate('ru', 'uk', $message_orig, 5);

            $send_data = [
                'text'   => $result,
                'reply_markup' => [
                    'resize_keyboard' => true,
                    'keyboard' => [
                        [
                            ['text' => '🇺🇦🇺🇦🇺🇦 ➡️➡️➡️ 💩🤢🤮']
                        ]
    
                    ]
                ]
            ];

        } else {
            $send_data = [
                'text'   => 'FAIL!!!'
            ];
        }
}

# Добавляем данные пользователя
$send_data['chat_id'] = $data['chat']['id'];
#Otppravlyaem otvet
$res = sendTelegram($method, $send_data);

//print_r($send_data);

function sendTelegram($method, $data, $headers = [])
{
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'https://api.telegram.org/bot' . TOKEN . '/' . $method,
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array_merge(array("Content-Type: application/json"), $headers)
    ]);   
    
    $result = curl_exec($curl);
    curl_close($curl);
    return (json_decode($result, 1) ? json_decode($result, 1) : $result);
}


#JSON function

function zapisJson($my_variable) {
	global $file;
    $content = json_encode($my_variable);
	file_put_contents($file, $content);
}



